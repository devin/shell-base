FROM rockylinux:8

# Versions
ENV K9S_VER    v0.25.18
ENV ASDF_VER   v0.9.0

# Add packages from yum
ADD yadm.repo /etc/yum.repos.d/yadm.repo
ADD shells-fish-release-3.repo /etc/yum.repos.d/shells-fish-release-3.repo
RUN yum -y update && yum -y install epel-release
RUN yum -y install fish tmux yadm sudo git vim-enhanced file tree \
    tig  passwd mutt irssi rtorrent lftp mtr jq ansible mosh nmap \
    rclone rpl wget net-tools iftop youtube-dl mariadb-common unzip \
    mtr \
    && yum clean all

RUN cd /tmp/; \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl

RUN cd /tmp/; \
    wget https://github.com/derailed/k9s/releases/download/${K9S_VER}/k9s_Linux_x86_64.tar.gz \
    && tar -zxf k9s_Linux_x86_64.tar.gz -C /usr/local/bin \
    && chmod +x /usr/local/bin/k9s

RUN rpm -ihv https://ftp.lysator.liu.se/pub/opensuse/ports/aarch64/tumbleweed/repo/oss/noarch/grc-1.13-1.2.noarch.rpm

# Set up the user, dotfiles and shell environment
RUN useradd devin -s /usr/bin/fish -G wheel

RUN sudo -u devin yadm clone https://code.vibechild.net/devin/dotfiles.git --bootstrap

RUN sudo -u devin /usr/bin/fish -c 'curl -sL https://git.io/fisher | source && fisher update'

# asdf package manager
RUN git clone https://github.com/asdf-vm/asdf.git /home/devin/.asdf --branch ${ASDF_VER}
RUN . /home/devin/.asdf/asdf.sh \
    && asdf plugin add helm \
    && asdf install helm latest \
    && asdf plugin add terraform \
    && asdf install terraform latest

# Grab vim plugins
RUN mkdir -p /Users/devin/.vim/pack/dist/start \
    && git clone https://github.com/vim-airline/vim-airline /home/devin/.vim/pack/dist/start/vim-airline \
    && git clone https://github.com/vim-airline/vim-airline-themes /home/devin/.vim/pack/dist/start/vim-airline-themes

# Make this repo available to reconfigure the image
RUN git clone https://code.vibechild.net/shared-images/shell-base.git /home/devin/image

RUN chown devin.devin -R /home/devin/
ADD devin.sudoers /etc/sudoers.d/devin
RUN chmod 0600 /etc/sudoers.d/devin

# https://github.com/insanum/gcalcli
#RUN pip install gcalcli

# https://github.com/erroneousboat/slack-term
RUN curl https://github.com/erroneousboat/slack-term/releases/download/v0.4.1/slack-term-linux-amd64 -L -o /usr/local/bin/slack-term && chmod +x /usr/local/bin/slack-term

# RUN mkdir /var/run/sshd
# RUN rm -f /var/run/nologin

CMD ["/usr/bin/fish"]
